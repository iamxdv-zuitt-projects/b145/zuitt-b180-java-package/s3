import java.util.HashMap;
import java.util.ArrayList;

public class Activity {

    /* A C T I V I T Y
    * 1. Create a HashMap of the following and their respective stock quantity.
    *   a. Mario Odyssey - 50
    *   b. Super Smash Bros. Ultimate- 20
    *   c. Luigi's Mansion 3 -15
    *   d. Pokemon Sword- 30
    *   e. Pokemon Shield- 100
    *       -> Note: use "Integer" instead of int for data type integer value/field in a HashMap.
    * 2. Loop over the items in the HashMap object and printout the contents.
    * 3. Create an array list called TopGames
    * 4. Loop over each item in the games hashmap and check if the quantity
    *   of the current item being iterated is less than or equal to 30
    *       a. if its, add the name of the game to our 'topGames' array list and show a message.
    * 5. Print the value of the 'topGmaes' array list the terminal.
    * */

    //S O L U T I O N:
    public static void main(String[] args){
        HashMap<String, Integer> itemStocks = new HashMap<>();
        itemStocks.put("Mario Odyssey", 50);
        itemStocks.put("Super Smash Bros. Ultimate", 20);
        itemStocks.put("Luigi's Mansion 3", 15);
        itemStocks.put("Pokemon Sword", 30);
        itemStocks.put("Pokemon Shield", 100);

        itemStocks.forEach((key, value) -> {
            System.out.println(key + " has " + value + " stocks left");
        });
        ArrayList<String> topGames = new ArrayList<>();
            itemStocks.forEach((key, value) -> {
                if (value <= 30) {
                    topGames.add(key);
                    System.out.println(key + " has been added to top games list");
                }
            });
        System.out.println("Our shop's Best seller games");
        System.out.println(topGames);
    }
}

