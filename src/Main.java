import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        //Loops are control structures that allow code blocks to be repeated according to conditions set.
        //Types of Loops
        //For Loop
        //While Loop
        //Do-While Loop

        //While Loop
        int a = 1;

        while (a < 5){

            System.out.println("While Loop Counter: " + a);
            a++;
        }

        int b = 5;

        do {
            System.out.println("Countdown: " + b);
            b--;
        } while (b > 10);

        //While Loop with User Input
        Scanner appScanner = new Scanner(System.in);
        String name = "";

        //isBlank() is a String method, which checks the non-whitespace characters in our string and will return true even if there is only a blank/whitespace.

        System.out.println(name.isEmpty());//will return false if there is at least whitespace in the string.
        System.out.println(name.isBlank());//will return true, if there is no characters or if the length of the string is 0, AND if the string contains a whitespace.

//        while(name.isBlank()){
//            System.out.println("What's your name?");
//            name = appScanner.nextLine();
//
//            if(!name.isBlank()){
//                System.out.println("Hi! " + name);
//            }
//        }

        //For Loops - are more versatile than our while loops
        for(int i = 1; i <= 10; i++){
            System.out.println("Count: " + i);
        }

        //For loop over a Java array
        int[] intArray = {100,200,300,400,500};
        for(int i = 0; i < intArray.length; i++){
            System.out.println("Item Index " + intArray[i]);
        }

        //Loop over Multidimensional Array
        //Multidimensional Arrays
        //A two-dimensional array, which can best be described by two lengths nested within each other like a matrix.
        //The first array could be for rows, the second could be for cols.
        String[][] classroom = new String[3][3];

        //First Row
        classroom[0][0] = "Rayquaza";
        classroom[0][1] = "Kyogre";
        classroom[0][2] = "Groudon";

        //second row
        classroom[1][0] = "Sora";
        classroom[1][1] = "Goofy";
        classroom[1][2] = "Donald";

        //third row
        classroom[2][0] = "Harry";
        classroom[2][1] = "Ron";
        classroom[2][2] = "Hermione";

        System.out.println(Arrays.toString(classroom));

        //Nested For Loop
        for(int row = 0; row < 3; row++){
            for(int col = 0; col < 3; col++){
                System.out.println(classroom[row][col]);
            }
        }

        //Enhanced For Loop for Java Array/ArrayList
        //In Java, a for-each loop can be used to iterate over the items of an array and arraylist.
        //for-each in java for array and arraylist is also called enhanced for loop.

        String[] members = {"Eugene","Vincent","Dennis","Alfred"};
        //member is a parameter which will represent each item in the given array
        /*
            for(DataType parameterName: arrayName){
                //code block will for each item in the array
            }
        */
        for(String member: members){
            System.out.println(member);
        }

        //Multidimensional Array for-each
        for(String[] row: classroom){
            //row = array
            for(String student: row){
                System.out.println(student);
            }
        }
        //HashMap forEach
        //HashMap has a method for iterating each field-value pair.
        //The HashMap forEach() requires a lambda expression as an argument.
        //A lambda expression in Java, is a short block of code which takes in paramters and returns a value. Lambda expressions are similar to methods, but they do not need a name and they can be implemented within another method.

        HashMap<String,String> techniques = new HashMap<>();
        techniques.put(members[0],"Spirit Gun");
        techniques.put(members[1],"Black Dragon");
        techniques.put(members[2],"Rose Whip");
        techniques.put(members[3],"Spirit Sword");
        System.out.println(techniques);

        techniques.forEach((key,value) -> {
            System.out.println("Member " + key + " uses " + value);
        });

        //Exception Handling (Scanner)
        //As the Scanner methods have specific dataTypes associated with them (nextLine(),nextInt(),nextDouble()), it is best to assume that there will be issues in user input especially if they fail to provide the correct data type.
        //Exception Handling refers to managing and catching run-time errors in order to safely run our code.

        System.out.println("Enter an integer:");
        int num = 0;

        //try-catch-finally - try-catch statement allows us to catch exceptions in our code.

        try {
            num = appScanner.nextInt();
        } catch (Exception e){
            System.out.println("Invalid Input");
            e.printStackTrace();
        }


        System.out.println(num);


    }
}
